import { RedisClient } from 'redis';
import { setupConf } from '../setup-conf';
import { ClientAuth } from '../client-auth';

const argv = require('minimist')(process.argv.slice(2));

if (!argv['id'] || argv.length > 1) {
    console.error('Usage: create-token.ts --id <client id>');
    process.exit(1);
}

try {
    const config = setupConf();
    config.required(['isbn:redis']);

    const redis = new RedisClient(config.get('isbn:redis'));

    const auth = new ClientAuth(config, redis);
    auth.init();

    console.log(auth.getToken(argv['id']));

    process.exit(0);
} catch (err) {
    console.error(err);
    process.exit(1);
}
