import { Express, Request } from 'express';
import { RedisClient } from 'redis';
import * as passport from 'passport';
import { Strategy as JwtStrategy, ExtractJwt, StrategyOptions } from 'passport-jwt';
import * as jwt from 'jsonwebtoken';
import { Provider } from 'nconf';
import { promisify } from 'util';
import * as uuid from 'uuid/v4';
import * as fs from 'fs';

interface Client {
    id: string,
    name: string,
    canAddNumbers: boolean
}

const clientPrefix = 'isbn:client:';

export class ClientAuth {
    private get: (key: string) => Promise<string>;
    private set: (key: string, value: string) => Promise<any>;
    private jwtSecret: string;

    constructor(private config: Provider, private redis: RedisClient) {
        this.get = promisify(redis.get).bind(redis);
        this.set = promisify(redis.set).bind(redis);
    }

    init() {
        const jwtFile = this.config.get('isbn:jwtSecretFile');

        if (jwtFile) {
            try {
                this.jwtSecret = fs.readFileSync(jwtFile).toString();
            } catch(err) {
                throw Error('Error reading ' + jwtFile + ': ' + err);
            }
        } else {
            this.jwtSecret = this.config.get('isbn:jwtSecret');
        }

        if (!this.jwtSecret) {
            throw Error('Can not initialize ClientAuth without a JWT secret');
        }

        let opts: StrategyOptions = {
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: this.jwtSecret,
        };

        passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
            if (!jwt_payload.sub) {
                done({message: 'Authorization error', status: 401});
            }

            this.redis.get(clientPrefix + jwt_payload.sub, (err, clientData) => {
                if (err) {
                    return done(Error('Database error'), false);
                }

                if (!clientData) {
                    return done({message: 'Client unknown', status: 401}, false);
                }

                return done(null, JSON.parse(clientData));
            });
        }));
    }

    async getClient(clientId: string): Promise<Client> {
        let client: Client;

        try {
            client = JSON.parse(await this.get(clientPrefix + clientId));
        } catch (err) {
            throw Error('Database error');
        }

        if (!client) {
            throw Error('Client ' + clientId + ' not found');
        }

        return client;
    }

    private async upsertClient(clientId: string, client: Client): Promise<any> {
        try {
            return await this.set(clientPrefix + clientId, JSON.stringify(client));
        } catch (err) {
            throw Error('Database error');
        }
    }

    async createClient(name: string, id?: string): Promise<string> {
        if (!id) {
            id = uuid();
        }

        const client: Client = {id: id, name: name, canAddNumbers: false};

        await this.upsertClient(id, client);
        return id;
    }

    clientCanAddNumbers(req: Request): boolean {
        return !!req && !!req.user && !!req.user.canAddNumbers;
    }

    async grantClientPostPermission(clientId: string): Promise<any> {
        const client = await this.getClient(clientId);

        client.canAddNumbers = true;

        return this.upsertClient(clientId, client);
    }

    async revokeClientPostPermission(clientId: string): Promise<any> {
        const client = await this.getClient(clientId);

        client.canAddNumbers = false;

        return this.upsertClient(clientId, client);
    }

    getToken(clientId): string {
        return jwt.sign({sub: clientId}, this.jwtSecret);
    }
}
