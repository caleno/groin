import * as nconf from 'nconf';
import { Provider } from 'nconf';

export function setupConf(): Provider {
    // init config
    let configFile;
    
    if (process.env.GROIN_CONFIG_FILE) {
        configFile = process.env.GROIN_CONFIG_FILE;
    } else {
        configFile = process.env.NODE_ENV === 'test' ? './config.test.json' : './config.json';
    }

    nconf.argv({separator: '-'})
    .env({separator: '_'})
    .file({ file: configFile });

    nconf.defaults({
        isbn: {
            host: 'localhost',
            jwtSecret: 'providethis!!',
            port: 3000,
            redis: {},
            email: {
                // by having 0 here, no emails will be sent in the default setup
                whenFewerThan: 0,
            }
        }
    });

    return nconf;
}
