import * as chai from 'chai';
import * as request from 'supertest';
import * as sinon from 'sinon';
import { SinonSandbox } from 'sinon';
import * as express from 'express';
import { Express} from 'express';
import * as bodyParser from 'body-parser';

import { ClientAuth } from './client-auth';
import { ISBNController } from './isbn-controller';

const expect = chai.expect;

describe('ISBN Controller', () => {

    /* istanbul ignore next */
    const mockRedis = {
        smembers: (key, cb) => cb(),
        spop: (key, cb) => cb(),
        sadd: (key, value, cb) => cb(),
        smove: (source, dest, value, cb) => cb(),
        sismember: (key, value, cb) => cb(null, false),
        scard: (key, cb) => cb(null, 0),
        get: (key, cb) => cb(),
        set: (key, value, cb) => cb()
    }

    /* istanbul ignore next */
    const mockConfig = {
        get: () => '',
        required: () => {}
    }

    const mockTransport = {
        sendMail: (value, cb) => cb()
    }

    let app: Express;
    let sandbox: SinonSandbox;
    let auth: ClientAuth;

    describe('Operations', () => {

        beforeEach(() => {
            auth = new ClientAuth(mockConfig as any, mockRedis as any);

            app = express();
            app.use(bodyParser.text({ type: 'text/csv' }));
            app.use(bodyParser.json());
            app.use(bodyParser.urlencoded({ extended: true }));
            app.use('/', ISBNController(mockConfig, mockRedis as any, auth, mockTransport as any));

            sandbox = sinon.createSandbox();
        });

        afterEach(() => sandbox.restore());

        describe('GET /', () => {
            it('should return the members of the ISBN set', async () => {
                const list = [ 'foo' ];
                sandbox.stub(mockRedis, 'smembers').callsFake((key, cb) => cb(null, list));

                await request(app).get('/')
                .expect(200)
                .expect('X-Total-Count', '1')
                .expect(res => expect(res.body).to.deep.equal(list));
            });

            it('should return 500 if redis SMEMBERS fails', async () => {
                sandbox.stub(mockRedis, 'smembers').callsFake((key, cb) => cb(Error('foo')));

                await request(app).get('/')
                .expect(500);
            });
        });

        describe('PUT /', () => {
            it('should block clients who are not allowed to post', async () => {
                sandbox.stub(auth, 'clientCanAddNumbers').returns(false);

                await request(app).put('/').expect(401);
            })

            it('should add members to the ISBN set', async () => {
                sandbox.stub(auth, 'clientCanAddNumbers').returns(true);

                // trying to cover most cases of ISBN-10 and 13, and checksums
                const list = [ '978-0-596-52068-7', '0-596-52068-9', '0-9752298-0-X', '960-425-059-0', '978-3-16-148410-0' ];

                await request(app).put('/')
                .send(list)
                .expect(200)
                .expect(res => expect(res.body).to.deep.equal({ message: 'accepted' }));
            });

            it('should allow posting csv', async () => {
                sandbox.stub(auth, 'clientCanAddNumbers').returns(true);

                const list = [ '978-0-596-52068-7', '0-596-52068-9', '0-9752298-0-X', '960-425-059-0', '978-3-16-148410-0' ];
                const csv = list.join('\n');

                await request(app).put('/')
                .send(csv)
                .set('Content-Type', 'text/csv')
                .expect(200);
            });

            it('should allow posting csv with windows line-breaks', async () => {
                sandbox.stub(auth, 'clientCanAddNumbers').returns(true);

                const list = [ '978-0-596-52068-7', '0-596-52068-9', '0-9752298-0-X', '960-425-059-0', '978-3-16-148410-0' ];
                const csv = list.join('\r\n');

                await request(app).put('/')
                .send(csv)
                .set('Content-Type', 'text/csv')
                .expect(200);
            });

            it('should allow posting csv with empty lines', async () => {
                sandbox.stub(auth, 'clientCanAddNumbers').returns(true);

                const list = [ '978-0-596-52068-7', '0-596-52068-9', '0-9752298-0-X', '960-425-059-0', '978-3-16-148410-0', '' ];
                const csv = list.join('\n');

                await request(app).put('/')
                .send(csv)
                .set('Content-Type', 'text/csv')
                .expect(200);
            });

            it('should strip numbers of dashes and prefixes before storing', async () => {
                sandbox.stub(auth, 'clientCanAddNumbers').returns(true);
                const spy = sandbox.spy(mockRedis, 'sadd');

                await request(app).put('/')
                .send([ '978-0-596-52068-7' ])
                .expect(200);

                expect(spy.calledOnce).to.be.true;
                expect(spy.args[0][1]).to.equal('9780596520687');
            });

            it('should return 500 if redis SADD fails', async () => {
                sandbox.stub(auth, 'clientCanAddNumbers').returns(true);

                sandbox.stub(mockRedis, 'sadd').callsFake((key, value, cb) => cb(Error('foo')));

                await request(app).put('/')
                .send([ '978-0-596-52068-7' ])
                .expect(500);
            });

            it('should refuse to add invalid ISBN values', async () => {
                sandbox.stub(auth, 'clientCanAddNumbers').returns(true);

                await request(app).put('/')
                .send([ 'foo' ])
                .expect(400);

                await request(app).put('/')
                .send([ '978-0-596-52068-6' ])
                .expect(400);

                await request(app).put('/')
                .send([ '0-596-52068-0' ])
                .expect(400);
            });

            it('should refuse to add malformatted lists', async () => {
                sandbox.stub(auth, 'clientCanAddNumbers').returns(true);

                await request(app).put('/')
                .send('foo')
                .expect(400);

                await request(app).put('/')
                .send({})
                .expect(400);

                await request(app).put('/')
                .send([1])
                .expect(400);
            });

            it('should refuse to add numbers that have already been used', async () => {
                sandbox.stub(auth, 'clientCanAddNumbers').returns(true);
                sandbox.stub(mockRedis, 'sismember').callsFake((key, value, cb) => cb(null, true));

                await request(app).put('/')
                .send([ '978-0-596-52068-7' ])
                .expect(409);
            });

            it('should return 500 if redis SISMEMBER failse', async () => {
                sandbox.stub(auth, 'clientCanAddNumbers').returns(true);
                sandbox.stub(mockRedis, 'sismember').callsFake((key, value, cb) => cb('foo'));

                await request(app).put('/')
                .send([ '978-0-596-52068-7' ])
                .expect(500);
            })
        });

        describe('POST /reserve', async () => {
            it('should pop an ISBN from the list and return it', async () => {
                sandbox.stub(mockRedis, 'spop').callsFake((key, cb) => cb(null, 'foo'));

                await request(app).post('/reserve')
                .expect(200)
                .expect(res => expect(res.body).to.deep.equal({isbn: 'foo'}));
            });

            it('should return 404 if the list is empty', async () => {
                sandbox.stub(mockRedis, 'spop').callsFake((key, cb) => cb(null, null));

                await request(app).post('/reserve')
                .expect(404);
            });

            it('should return 500 if redis SPOP fails', async () => {
                sandbox.stub(mockRedis, 'spop').callsFake((key, cb) => cb('foo'));

                await request(app).post('/reserve')
                .expect(500);
            });

            it('should return 500 if redis SADD fails', async () => {
                sandbox.stub(mockRedis, 'spop').callsFake((key, cb) => cb(null, 'foo'));
                sandbox.stub(mockRedis, 'sadd').callsFake((key, value, cb) => cb('foo'));

                await request(app).post('/reserve')
                .expect(500);
            });
        });

        describe('POST /release/:isbn', () => {
            it('should release an ISBN from the used list and make it available again', async () => {
                const spy = sandbox.spy(mockRedis, 'smove');
                sandbox.stub(mockRedis, 'sismember').callsFake((key, value, cb) => cb(null, true));

                await request(app).post('/release/foo')
                .expect(200)
                .expect(res => expect(res.body.message).to.contain('foo'));

                expect(spy.called);
                expect(spy.args[0][2]).to.equal('foo');
            });

            it('should return 404 if attempting to release non-existent ISBN', async () => {
                await request(app).post('/release/foo')
                .expect(404)
            });

            it('should return 500 if redis SISMEMBER fails', async () => {
                sandbox.stub(mockRedis, 'sismember').callsFake((key, value, cb) => cb('foo'));

                await request(app).post('/release/foo')
                .expect(500)
            });

            it('should return 500 if redis SMOVE fails', async () => {
                sandbox.stub(mockRedis, 'sismember').callsFake((key, value, cb) => cb(null, true));
                sandbox.stub(mockRedis, 'smove').callsFake((source, dest, value, cb) => cb('foo'));

                await request(app).post('/release/foo')
                .expect(500)
            });
        });
    });

    describe('Email notifications', () => {
        const emailConfig = {
            whenFewerThan: 1,
            from: 'foo@bar.com',
            to: 'bar@foo.com'
        }

        beforeEach(() => {
            sandbox = sinon.createSandbox();
            sandbox.stub(mockConfig, 'get').withArgs('isbn:email').returns(emailConfig);

            auth = new ClientAuth(mockConfig as any, mockRedis as any);

            app = express();
            app.use(bodyParser.json());
            app.use(bodyParser.urlencoded({ extended: true }));
            app.use('/', ISBNController(mockConfig, mockRedis as any, auth, mockTransport as any));
        });

        afterEach(() => sandbox.restore());

        describe('POST /reserve', () => {

            it('should not send any email when enough numbers left', async () => {
                sandbox.stub(mockRedis, 'spop').callsFake((key, cb) =>  cb(null, 'foo'));
                sandbox.stub(mockRedis, 'scard').callsFake((key, cb) => cb(null, emailConfig.whenFewerThan + 1));
                const spy = sandbox.spy(mockTransport, 'sendMail');

                await request(app).post('/reserve')
                .expect(200)
                .expect(res => expect(res.body).to.deep.equal({isbn: 'foo'}));

                expect(spy.notCalled).to.be.true;
            });

            it('should send email when too few numbers left', async () => {
                sandbox.stub(mockRedis, 'spop').callsFake((key, cb) =>  cb(null, 'foo'));
                sandbox.stub(mockRedis, 'scard').callsFake((key, cb) => cb(null, emailConfig.whenFewerThan - 1));
                const mailSpy = sandbox.stub(mockTransport, 'sendMail').callsFake((message, cb) => cb(null, {envelope: 'foo' }));
                const consoleSpy = sandbox.stub(console, 'log'); // by stubbing we hide the message from the test log

                await request(app).post('/reserve')
                .expect(200)
                .expect(res => expect(res.body).to.deep.equal({isbn: 'foo'}));

                expect(mailSpy.calledOnce, 'should send message').to.be.true;
                expect(mailSpy.args[0][0]).to.have.property('from', emailConfig.from, 'should be from right address');
                expect(consoleSpy.calledOnce, 'should log email').to.be.true;
            });

            it('should succeed but log error if email fails to send', async () => {
                sandbox.stub(mockRedis, 'spop').callsFake((key, cb) =>  cb(null, 'foo'));
                sandbox.stub(mockRedis, 'scard').callsFake((key, cb) => cb(null, emailConfig.whenFewerThan - 1));
                sandbox.stub(mockTransport, 'sendMail').callsFake((message, cb) => cb('foo'));
                const consoleSpy = sandbox.stub(console, 'error');

                await request(app).post('/reserve')
                .expect(200)
                .expect(res => expect(res.body).to.deep.equal({isbn: 'foo'}));

                expect(consoleSpy.calledOnce, 'should log error').to.be.true;
            });

            it('should succeed but log error if redis SCARD fails', async () => {
                sandbox.stub(mockRedis, 'spop').callsFake((key, cb) =>  cb(null, 'foo'));
                sandbox.stub(mockRedis, 'scard').callsFake((key, cb) => cb('foo'));
                const consoleSpy = sandbox.stub(console, 'error');

                await request(app).post('/reserve')
                .expect(200)
                .expect(res => expect(res.body).to.deep.equal({isbn: 'foo'}));

                expect(consoleSpy.calledOnce, 'should log error').to.be.true;
            });
        });
    });
});
