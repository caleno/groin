import { setupConf } from './src/setup-conf';
import { start } from './src/server';

start(setupConf(), (err, app) => {
	console.log('ISBN server listening on ' + app.get('host') + ':' + app.get('port'))
});
