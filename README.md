# GrOIN - Grab Your Own ISBN

Minimalistic Node.js app to maintain a list of available ISBN numbers for use by other applications.
A client can get the list of unused ISBN numbers, add more numbers to the list (if authorized),
reserve a number and release a number that was reserved.
A reservation means the number is not available anymore, and can be indefinite.

The API has the following endpoints

    GET /api/isbn                 # get list of available isbn/numbers
    PUT /api/isbn                 # add new numbers to list
    POST /api/isbn/reserve        # reserve an ISBN number
    POST /api/isbn/release/:isbn. # release an ISBN number previously reserved

Authentication is done using bearer tokens, see below how to generate a new token.
When the database has fewer than a certain number of ISBN numbers available,
a warning email will be sent each time a number is reserved.

`PUT /api/isbn` can only be called by users with special priveleges.
See below how to grant these. It accepts both a JSON array of ISBN numbers

```
PUT /api/isbn
Authorization: Bearer abc.efg.123
Content-Type: application/json
Content-Length: 40

[ "978-0-596-52068-7", "0-596-52068-9" ]
```

and a CSV file (i.e. a plain text list)

```
PUT /api/isbn
Authorization: Bearer abc.efg.123
Content-Type: text/csv
Content-Length: 31

978-0-596-52068-7
0-596-52068-9
```

Both these will give the following for a subsequent call to `GET /api/isbn`

```
[ "9780596520687", "0596520689" ]
```

Both ISBN-10 and ISBN-13 will be accepted. The application checks that
the numbers are valid, and strips away dashes and prefixes to avoid duplication.
If the same number is posted twice it will only be stored once.

## Setting up the application

Clone the repository and run:

    npm install

Then install redis and start it up. Then run:

    npm start

and the server will be available at http://localhost:3000 (assuming you have the
default redis setup).

The application can be configured using a config-file, `config.json` in the app
root directory, or by environment variables. Sample config:

```json
{
    "isbn": {
        "host": "isbn.example.com",
        "port": 1234,
        "jwtSecret": "a long and random string",
        "jwtSecretFile": "/path/to/<file containing the JWT secret (this option has precedence)>",
        "redis": {
            "port": 6379,
            "host": "localhost",
            "any": "other parameter that redis accepts"
        },
        "email": {
            "from": "isbn@your-server.com",
            "to": "admin@your-server.com",
            "whenFewerThan": "When there are fewer numbers left in the database than the number here, emails will be sent",
            "nodemailer": {
                "sendmail": "true",
                "or": "the nodemailer config you want"
            }
        }
    }
}
```

The file is read using the module nconf, and the same values can be provided as environment
variables in the format `isbn_host=isbn.example.com` and `isbn_redis_port=6379`, etc.
The application can be started without the email section, the only thing that happens is that
no emails will be sent.

In production your redis server must be [persistent](https://redis.io/topics/persistence).


## Running using docker compose

The whole application, including a redis instance can also be run using docker-compose.
For production use, the jwt secret needs to be stored in a file. Start the application
using

    docker-compose up

and it will be available at http://localhost:3000. This uses a persistent redis setup.
For this to work, the environment variable `ISBN_JWT_SECRET_FILE` must be specified.
(path to file with JWT secret). In addition the following variables should be included
to enable email sending.

    ISBN_EMAIL_FROM           # sender
    ISBN_EMAIL_TO             # recipient
    ISBN_EMAIL_WHENFEWERTHAN  # threshold for sending emails


## Adding and changing clients

Before you can make any calls to the server, you need create API clients and authentication tokens.
These can be added through the command line. To create a new client, call

    > $(npm bin)/ts-node src/cli/create-client --name 'client name'
    Client created with id: adbbc6e9-4ad5-4e47-84fe-e260fd7e82a8

To create a JWT token to be used for API requests for the client, call

    > $(npm bin)/ts-node src/cli/create-token --id adbbc6e9-4ad5-4e47-84fe-e260fd7e82a8
    abc123.def456.ghi789

To grant a client permission to add new ISBN numbers to the list of available numbers, call

    > $(npm bin)/ts-node src/cli/grant-post-permission --id adbbc6e9-4ad5-4e47-84fe-e260fd7e82a8
    Permission granted

To revoke the same permission, call

    > $(npm bin)/ts-node src/cli/revoke-post-permission --id adbbc6e9-4ad5-4e47-84fe-e260fd7e82a8
    Permission revoked

Using docker-compose, the same four commands will be transformed to

    > docker-compose exec app ./node_modules/.bin/ts-node src/cli/create-client ...

`ISBN_JWT_SECRET_FILE` must be specified here as well.


## Tests

To run tests have a test instance of redis running and type `npm test` (expects standard setup).
This will run both integration and unit tests. The test config uses nconf and variables can be supplied
in a file `config.test.json`, or through environment variables (see above).
The integration tests will flush the redis database, so use a test instance.
